package br.com.pudim.blockbusterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
 
@SpringBootApplication
public class BlockbusterApiApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(BlockbusterApiApplication.class, args);
	}

}
