package br.com.pudim.blockbusterapi.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import br.com.pudim.blockbusterapi.models.Movie;
import br.com.pudim.blockbusterapi.models.User;

@CrossOrigin
@RequestMapping("/api")
@RestController
public class Controller {

	@GetMapping("/")
	public ResponseEntity<?> helloWorld(){
		return new ResponseEntity<String>("Hello World!", HttpStatus.OK);
	}
	
	@GetMapping("/moviesandusers")
	public ResponseEntity<?> returnMovies(){
		//abrindo conexão com o banco
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("Blockbuster");

		EntityManager manager = factory.createEntityManager();
		
		//criando json da resposta
		JsonObject response = new JsonObject();
		
		//fazendo consulta para retornar todos os filmes
		String sql = "select m from Movie m";
		TypedQuery query = manager.createQuery(sql, Movie.class);
		List<Movie> movies = query.getResultList();
		
		
		for(int cc = 0 ; cc < movies.size() ; cc++) {
			//pegando cada filme
			Movie movie = movies.get(cc);
			//criando array onde vao ficar os usuarios
			JsonArray users_movie = new JsonArray();
			
			//fazendo query de usuarios pelo filme 
			String sql_ = "select u from User u where u.filme = :movie_id";
			TypedQuery query_ = manager.createQuery(sql_, User.class)
					.setParameter("movie_id", movie.getId());
			List<User> users = query_.getResultList();
			
			//adiciona os usuarios no array
			for(int tt = 0; tt < users.size(); tt++) {
				users_movie.add(users.get(tt).getNome() + " " + users.get(tt).getSobrenome());
			}
			
			//adiciona o titulo do filme e o array com as pessoas que alugaram ao json de resposta
			response.add(movie.getTitulo(), users_movie);
		}
		
		manager.close();
		factory.close();
		
		return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> returnMovies(@PathVariable("id") int id){
		//abrindo conexão com o banco
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("Blockbuster");

		EntityManager manager = factory.createEntityManager();
		
		//criando json de resposta
		JsonObject response = new JsonObject();
		
		//procurando o usuario com o id fornecido
		String sql = "select u from User u where u.id = :id";
		TypedQuery<User> query = manager.createQuery(sql, User.class)
				.setParameter("id", id);
		User user = query.getSingleResult();
		
		//procurando filme que o usuário alugou
		String sql_ = "select m from Movie m where m.id = :id";
		TypedQuery<Movie> query_ = manager.createQuery(sql_, Movie.class)
				.setParameter("id", user.getFilme());
		Movie movie = query_.getSingleResult();
		
		//montando json de resposta com dados do usuario
		response.addProperty("id", user.getId());
		response.addProperty("Nome", user.getNome());
		response.addProperty("Sobrenome", user.getSobrenome());
		
		//criando json com dados do filme
		JsonObject json_movie = new JsonObject();
		
		//montando json com dados do filme
		json_movie.addProperty("id", movie.getId());
		json_movie.addProperty("Titulo", movie.getTitulo());
		json_movie.addProperty("Categoria", movie.getCategoria());
		
		//adicionando json com dados do filme ao json da resposta
		response.add("Filme", json_movie);
		
		manager.close();
		factory.close();
		
		return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
	}
}
